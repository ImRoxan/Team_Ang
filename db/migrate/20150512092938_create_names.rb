class CreateNames < ActiveRecord::Migration
  def change
    create_table :names do |t|
      t.string :product_name
      t.string :price
      t.integer :stock
      t.text :description

      t.timestamps null: false
    end
  end
end
