class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.all
  end

  def show
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.all
  end

  def create
    @user = User.new(params[user_params])

    if @user.save
      redirect_to @user
    end
  else
    render 'new'
  end

  def update
    @user = User.find(params[:id])

    if @user.update(user_params)
      redirect_to  @user
    else
      render 'edit'
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy

    redirect_to users_path
  end

  private
  def user_params
    params.require(:users).permit(:user, :price)
  end
end