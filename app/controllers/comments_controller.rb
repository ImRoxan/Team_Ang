class CommentsController < ApplicationController
  def create
    @menu = Menu.find(params[:menu_id])
    @comment = @menu.comments.create(comment_params)
    redirect_to menus_path(@menu)
  end

  def destroy
    @article = Menu.find(params[:article_id])
    @comment = @menu.comments.find(params[:id])
    @comment.destroy
    redirect_to menus_path(@menu)
  end

  private
  def comment_params
    params.require(:comment).permit(:commenter, :body)
  end
end
