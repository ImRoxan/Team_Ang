class CartsController < ApplicationController
  before_action :authenticate_user!

  def cart_action(current_user_id)
    if $redis.sismember "cart#{current_user_id}", :id
      "Remove from"
    else
      "Add to"
    end
  end
  def show

  end


  def add
    $redis.sadd current_user_cart, params[:product_id]
    render json: current_user.cart_count, status: 200
  end

  def remove
    $redis.srem current_user_cart, params[:product_id]
    render json: current_user.cart_count, status: 200
  end

  private

  def current_user_cart
    "cart#{current_user.id}"
  end


end