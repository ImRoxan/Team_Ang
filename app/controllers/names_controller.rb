class NamesController < ApplicationController
  def create
    @post_attachment = PostAttachment.find(params[:post_attachment_id])
    @name = @post_attachment.names.create(name_params)
    redirect_to post_attachments_path(@post_attachment)
  end

  private
  def name_params
    params.require(:name).permit(:commenter, :body)
  end
end
